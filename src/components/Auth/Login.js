import React from "react";
import { Row, Col, Input, Button } from "antd";
import { Link } from "react-router-dom";
import AuthLayout from "./AuthLayout";

const Login = () => {
  return (
    <>
      <AuthLayout fadeName="fade-right" pageTitle="Login" googleLabel="Sign In">
        <form>
          <Row gutter={24}>
            <Col xs={24} xl={24} lg={24}>
              <div className="form-group">
                <label>Email Address</label>
                <Input />
              </div>
            </Col>
            <Col xs={24} xl={24} lg={24}>
              <div className="form-group">
                <label>Password</label>
                <Input.Password />
              </div>

              <p className="p-pwd">
                <Link to="/forgot-password">Forgot Password?</Link>
              </p>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xs={24} xl={24} lg={24}>
              <Button block type="primary">
                LOGIN
              </Button>
            </Col>
          </Row>
          <Row>
            <Col xs={24} xl={24} lg={24}>
              <p className="register">
                Don't have account? <Link to="/register">Register</Link>
              </p>
            </Col>
          </Row>
        </form>
      </AuthLayout>
    </>
  );
};

export default Login;
