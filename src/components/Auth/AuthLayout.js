import React, { useEffect } from "react";
import { Row, Col, Card, Divider } from "antd";
import GoogleIcon from "../../assets/images/gmailIcon.svg";
import { useHistory } from "react-router-dom";
import { Helmet } from "react-helmet";
import AOS from "aos";
// import authBg1 from "../../assets/img/auth/corn-on-the-cob-2083529_1280.jpg";
// import authBg2 from "../../assets/img/auth/BB760D04-0DC4-4349-BFD1-641529BE0A78.jpg";
// import authBg3 from "../../assets/img/auth/WheatVarietyTrials_HeadedGreen.jpg";
// import authBg4 from "../../assets/img/auth/image_22.jpg";

const AuthLayout = ({ children, style, fadeName, googleLabel, pageTitle }) => {
  const history = useHistory();
  const { location } = history;

  useEffect(() => {
    AOS.init({ duration: 600 });
  }, []);

  // const bgImages = [authBg1, authBg2, authBg3, authBg4];

  return (
    <>
      <Helmet>
        <title>{pageTitle} | GIS</title>
      </Helmet>
      <Row>
        <Col xs={24} xl={12} lg={12}>
          <div className="left-side">
            {/* <BackgroundSlider images={bgImages} duration={5} transition={2} /> */}
            <div className="overlay"></div>
            <div className="left-content">
              <h2>Manage your fields remotely</h2>
              <p>
                Monitor the state of your crops right from the office, learn
                about the slightest changes on-the-spot, and make fast and
                reliable decisions on field treatment
              </p>
            </div>
          </div>
        </Col>
        <Col xs={24} xl={12} lg={12}>
          <div className="right-side">
            <div className="auth-form" style={{ style }}>
              <Card
                bordered={false}
                className="card-shadow"
                data-aos={fadeName}
                data-aos-offset="300"
                data-aos-easing="ease-in-sine"
              >
                {location.pathname === "/login" ||
                location.pathname === "/register" ? (
                  <>
                    <div
                      className="auth_page_writeup"
                      style={{ color: "#011C14" }}
                    >
                      <h2>Welcome Back</h2>
                      <p>
                        Stay ahead alongside 6300 successful farmers managing
                        over 10000 acres in Crop Monitoring
                      </p>
                    </div>
                    <Row gutter={24}>
                      <Col xs={24} xl={24} lg={24}>
                        <button className="g-login">
                          <img src={GoogleIcon} alt="gooleicon" />
                          <span>{googleLabel} with Google</span>
                        </button>
                      </Col>
                    </Row>
                    <Divider>OR</Divider>
                  </>
                ) : (
                  ""
                )}
                {children}
              </Card>
            </div>
          </div>
        </Col>
      </Row>
    </>
  );
};

export default AuthLayout;
