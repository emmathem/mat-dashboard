import React, { useState } from "react";
import AuthLayout from "./AuthLayout";
import { Row, Col, Input, Button } from "antd";
import { Link, useHistory } from "react-router-dom";
import SuccessMessage from "./SuccessMessage";
import ArrowRightAltIcon from '@material-ui/icons/ArrowRightAlt';

const ForgotPassword = () => {
  const history = useHistory();
  const [loading, setloading] = useState(false);
  const resetPasswordLink = () => {
    setTimeout(() => {
      setloading(true);
    }, 2000);
  };

  const gotoResetPassword = () => {
    history.push("/reset-password/059585959584");
  };
  const styles = {
      marginTop: '4rem',
  };
  return (
    <>
      <AuthLayout style={styles} fadeName="fade-left" pageTitle="Forgot Password">
        {!loading ? (
          <>
            <div className="auth-title">
              <h2>Recover Password</h2>
              <p>Enter your regsitered email address to reset your password</p>
            </div>
            <Row gutter={24} style={{ marginTop: '4rem' }}>
              <Col xs={24} xl={24} lg={24}>
                <div className="form-group">
                  {/* <label>Email Address</label> */}
                  <Input placeholder="Email Address" />
                </div>
              </Col>
            </Row>
            <Row gutter={24}>
              <Col xs={24} xl={24} lg={24}>
                <Button onClick={resetPasswordLink} block type="primary">
                  SEND LINK
                </Button>
              </Col>
            </Row>
            <Row>
              <Col xs={24} xl={24} lg={24}>
                <p className="register">
                  <Link to="/login"> <ArrowRightAltIcon /> Return to Login</Link>
                </p>
              </Col>
            </Row>
          </>
        ) : (
          <SuccessMessage
            description="Code has been sent to your mail, check your inbox"
            successTitle="SUCCESSFULLY SENT"
            action={gotoResetPassword}
          />
        )}
      </AuthLayout>
    </>
  );
};

export default ForgotPassword;
