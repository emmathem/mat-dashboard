import React from "react";
import { Row, Col, Input, Button } from "antd";
import { Link } from "react-router-dom";
import AuthLayout from "./AuthLayout";

const Login = () => {
  return (
    <>
      <AuthLayout
        fadeName="fade-left"
        googleLabel="Sign Up"
        pageTitle="Register"
      >
        <form>
          <Row gutter={24}>
            <Col xs={24} xl={12} lg={12}>
              <div className="form-group">
                {/* <label>First Name</label> */}
                <Input placeholder="First Name" />
              </div>
            </Col>
            <Col xs={24} xl={12} lg={12}>
              <div className="form-group">
                {/* <label>Last Name</label> */}
                <Input placeholder="Last Name" />
              </div>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xs={24} xl={24} lg={24}>
              <div className="form-group">
                {/* <label>Email Address</label> */}
                <Input placeholder="Email Address" />
              </div>
            </Col>
            <Col xs={24} xl={24} lg={24}>
              <div className="form-group">
                {/* <label>Password</label> */}
                <Input.Password placeholder="Password" />
              </div>
            </Col>
          </Row>
          <Row gutter={24}>
            <Col xs={24} xl={24} lg={24}>
              <Button block type="primary">
                REGISTER
              </Button>
            </Col>
          </Row>
          <Row>
            <Col xs={24} xl={24} lg={24}>
              <p className="register">
                Don't have account? <Link to="/login">Sign in</Link>
              </p>
            </Col>
          </Row>
        </form>
      </AuthLayout>
    </>
  );
};

export default Login;
