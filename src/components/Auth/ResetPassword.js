import React, { useState } from "react";
import AuthLayout from "./AuthLayout";
import { Row, Col, Input, Button } from "antd";
import { useHistory, useParams } from "react-router-dom";

const ResetPassword = () => {
  const [resetload, setResetload] = useState(false);
  const history = useHistory();

  console.log(useParams());
  const resetPassword = () => {
    setTimeout(() => {
      setResetload(true);
    }, 3000);
    history.push("/login");
  };
  const styles = {
    marginTop: '4rem',
};
  return (
    <>
      <AuthLayout style={styles} fadeName="fade-left" pageTitle="Reset Password">
        <div className="auth-title" style={{ marginBottom: "20px" }}>
          <h2>Reset Password</h2>
          <p>Enter your new password</p>
        </div>
        <Row gutter={24}>
          <Col xs={24} xl={24} lg={24}>
            <div className="form-group">
              <Input placeholder="Email Password" />
            </div>
          </Col>
          <Col xs={24} xl={24} lg={24}>
            <div className="form-group">
              <Input placeholder="Confirm Password" />
            </div>
          </Col>
        </Row>
        <Row gutter={24}>
          <Col xs={24} xl={24} lg={24}>
            <Button
              loading={resetload}
              onClick={resetPassword}
              block
              type="primary"
            >
              RESET PASSWORD
            </Button>
          </Col>
        </Row>
      </AuthLayout>
    </>
  );
};

export default ResetPassword;
