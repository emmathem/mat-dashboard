import React from "react";
import { Calendar, momentLocalizer } from "react-big-calendar";
import DashboardLayout from "../../Dashboard/DashboardLayout";
import moment from "moment";

const ManageEvents = () => {
  const localizer = momentLocalizer(moment); // or globalizeLocalizer
  //   const now = new Date()
  const myEventsList = [
    {
      id: 0,
      title: "All Day Event very long title",
      allDay: true,
      start: moment(),
      end: '2021-06-28',
    },
  ];
  return (
    <DashboardLayout>
      <Calendar
        localizer={localizer}
        events={myEventsList}
        startAccessor="start"
        endAccessor="end"
        style={{ height: 500 }}
      />
    </DashboardLayout>
  );
};

export default ManageEvents;
