import styled from "styled-components";

export const HeaderContainer = styled.header`
  background: var(--primary-green-color);
  display: flex;
  position: fixed;
  width: 100%;
  height: 43px;
  padding: 5px 10px;
  transition: ease all 0.5s;
  align-items: center;
  justify-content: space-between;
  flex-wrap: wrap;
  z-index: 1;
  .menu-open {
    position: relative;
    padding-left: 5rem;
    color: #fff;
    display: flex;
    align-items: center;
    transition: ease all 0.5s;
    cursor: pointer;
    svg {
      font-size: 2rem;
    }
  }
  .profile {
    transition: ease all 0.5s;
    margin-right: 3rem;
    position: relative;
    display: flex;
    align-items: center;
    cursor: pointer;
    &__avatar {
      width: 40px;
      height: 40px;
      background: #ccc;
      border-radius: 50%;
      display: flex;
      justify-content: center;
      align-items: center;
      overflow: hidden;
      span {
        font-weight: bold;
        color: var(--menu-color);
        font-size: 1rem;
      }
    }
    &__name {
      margin-left: 5px;
      color: var(--white-color);
      font-size: small;
    }
    &__icon {
      svg {
        fill: var(--white-color);
      }
    }
  }
`;

export const MainContainer = styled.main`
  position: relative;
  padding-top: 3rem;
  background-color: #c3f7e7;
  height: 100%;
  .test {
    display: flex;
    flex-direction: row;
    align-items: center;
    justify-content: space-between;
    padding: 0 30px 0 0;
  }
`;
