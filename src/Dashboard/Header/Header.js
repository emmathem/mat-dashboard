import React, { useState } from "react";
import { IconButton, Toolbar, AppBar, CssBaseline, Menu, MenuItem } from "@material-ui/core";
import { ArrowDropDown, ExitToApp, People, Person, Home } from "@material-ui/icons";
import { Link, useHistory } from "react-router-dom";
import MenuIcon from '@material-ui/icons/Menu';
import clsx from "clsx";

const Header = (props) => {
  const history = useHistory();
  const [anchorEl, setAnchorEl] = useState(null);

  const handleClick = (event) => {
    setAnchorEl(event.currentTarget);
  };

  const handleClose = () => {
    setAnchorEl(null);
  };

  const login = () => {
    history.push('/login');
  };
  return (
     <>
      <CssBaseline />
      <AppBar
        position="fixed"
        className={clsx(props.classes.appBar, {
          [props.classes.appBarShift]: props.open,
        })}
      >
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={props.handleDrawerOpen}
            edge="start"
            className={clsx(props.classes.menuButton, {
              [props.classes.hide]: props.open,
            })}
          >
            <MenuIcon />
          </IconButton>
        </Toolbar>
        <div className="profile" onClick={handleClick}>
          <div className="profile__avatar">
            <span>P.T</span>
          </div>
          <div className="profile__name">
            Welcome, <br />
            <strong>Peter Tokunbo</strong>
          </div>
          <div className="profile__icon">
            <ArrowDropDown />
          </div>
        </div>
        <Menu
          id="simple-menu"
          anchorEl={anchorEl}
          keepMounted
          open={Boolean(anchorEl)}
          onClose={handleClose}
        >
          <MenuItem onClick={handleClose}>
            <Link to="/">
              <Home /> Home
            </Link>
          </MenuItem>
          <MenuItem onClick={handleClose}>
            <Link to="/manage-role">
              <People /> Manage Role
            </Link>
          </MenuItem>
          <MenuItem onClick={handleClose}>
            <Link to="/manage-user">
              <Person /> My Account
            </Link>
          </MenuItem>
          <MenuItem onClick={handleClose}>
            <div className="logout" onClick={login}>
              <ExitToApp /> <span>Logout</span>
            </div>
          </MenuItem>
        </Menu>
      </AppBar>
     </>
  );
};

export default Header;
