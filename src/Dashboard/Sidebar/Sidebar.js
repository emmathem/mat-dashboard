import React from "react";
import clsx from "clsx";
// import List from "@material-ui/core/List";
import {
  Typography,
  IconButton,
  Accordion,
  Divider,
  Drawer,
  AccordionSummary,
  AccordionDetails,
} from "@material-ui/core";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import ChevronRightIcon from "@material-ui/icons/ChevronRight";
import { ExpandMore } from "@material-ui/icons";

const Sidebar = (props) => {
  const [expanded, setExpanded] = React.useState(false);

  const handleChange = (panel) => (event, isExpanded) => {
    setExpanded(isExpanded ? panel : false);
  };
  return (
    <>
      <Drawer
        variant="permanent"
        className={clsx(props.classes.drawer, {
          [props.classes.drawerOpen]: props.open,
          [props.classes.drawerClose]: !props.open,
        })}
        classes={{
          paper: clsx({
            [props.classes.drawerOpen]: props.open,
            [props.classes.drawerClose]: !props.open,
          }),
        }}
      >
        <div className={props.classes.toolbar}>
          <IconButton onClick={props.handleDrawerClose}>
            {props.theme.direction === "rtl" ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </div>
        <Divider />
        <div style={{ padding: "0 10px" }}>
          <Accordion
            expanded={expanded === "panel1"}
            onChange={handleChange("panel1")}
          >
            <AccordionSummary
              expandIcon={<ExpandMore />}
              aria-controls="panel1bh-content"
              id="panel1bh-header"
            >
              <Typography className={props.classes.heading}>
                Farm Management
              </Typography>
            </AccordionSummary>
            <AccordionDetails>
              <ul>
                  <li>Configure Farm Profile</li>
                  <li>Configure Farm Activities</li>
              </ul>
            </AccordionDetails>
          </Accordion>
        </div>
      </Drawer>
    </>
  );
};

export default Sidebar;
