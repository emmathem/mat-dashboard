import React from "react";
import DashboardLayout from "./Dashboard/DashboardLayout";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import ManageRole from "./components/ManageRole/ManageRole";
import ManageUser from "./components/ManageUser/ManageUser";
import Login from "./components/Auth/Login";
import Register from "./components/Auth/Register";
import ForgotPassword from "./components/Auth/ForgotPassword";
import ResetPassword from "./components/Auth/ResetPassword";
import ManageEvents from "./components/ManageUser/ManageEvents";

const App = () => {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={DashboardLayout} />
        <Route exact path="/login" component={Login} />
        <Route exact path="/register" component={Register} />
        <Route exact path="/forgot-password" component={ForgotPassword} />
        <Route exact path="/reset-password/:token" component={ResetPassword} />

        <Route exact path="/manage-role" component={ManageRole} />
        <Route exact path="/manage-user" component={ManageUser} />
        <Route exact path="/manage-event" component={ManageEvents} />
      </Switch>
    </Router>
  );
};

export default App;
